//
//  APIClient.swift
//  SwiftDays
//
//  Created by Eric Nilsson on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import Foundation

class APIClient : NSObject {
    typealias ApiCallback = [Song] -> ()
    
    class func loadSongs(callback: ApiCallback) {
        println("\(NSDate()) Loading songs...")
        
        let request = NSURLRequest(URL: NSURL(string: "http://178.62.227.29"))
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            if (error != nil) {
                println(error)
            }
            
            var parsingError: NSError?
            let jsonDictonary = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parsingError) as NSDictionary
            
            if (parsingError != nil) {
                println(parsingError)
            }
            
            let jsonSongs = jsonDictonary["songs"] as NSArray
            
            var songs = [Song]()
            
            for jsonSong in jsonSongs {
                let title = jsonSong["title"] as String
                let artist = jsonSong["artist"] as String
                let song = Song(artist: artist, title: title, coverArtUrl: nil, uri: nil)
                
                songs.append(song)
            }
            
            callback(songs.reverse())
        }
    }
}