//
//  PlaylistTableViewController.swift
//  SwiftDays
//
//  Created by Veronika Ekström on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import UIKit

class PlaylistTableViewController: UITableViewController {
    let dataSource = PlaylistDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.backgroundColor = UIColor(red: 0.137, green: 0.486, blue: 0.333, alpha:1)
        
        self.dataSource.tableView = self.tableView
        self.tableView.dataSource = dataSource
        
        self.tableView?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.dataSource.cellId)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("song_segue", sender: self)
    }
    
    @IBAction func openSettingsButtonClicked(sender:AnyObject) {
        NSLog("inställningar")
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
       if(segue.identifier != "song_segue"){
            return
        }
        
        if let indexPath = self.tableView.indexPathForSelectedRow() {
            let item = self.dataSource.itemAtIndexPath(indexPath)
            
            (segue.destinationViewController as SongInformationViewController).song = item
            
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.update()
        
        let timer = NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    func update() {
        APIClient.loadSongs { (songs) in
            self.dataSource.songs = songs
        }
    }
}
