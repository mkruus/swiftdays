//
//  SongInformationViewController.swift
//  SwiftDays
//
//  Created by Veronika Ekström on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//


import UIKit

class SongInformationViewController: UIViewController, SPTAudioStreamingDelegate {
    
    @IBOutlet var songInformationView:SongInformationView?
    
    let audioStreamingController = SPTAudioStreamingController()
    
    var song:Song = Song() {
        didSet {
            NSLog("Text: %@", self.song.description())
            
            let spotifyClient = SpotifyAPIClient(title: self.song.title, artist: self.song.artist)            
            spotifyClient.request() { (newSong) in
                NSLog(newSong.description + " " + (newSong.coverArtUrl?.absoluteString)!)

                self.songInformationView?.song = newSong
                
                var sessionData = NSUserDefaults.standardUserDefaults().objectForKey(kSessionUserDefaultsKey) as NSData?
                let session = NSKeyedUnarchiver.unarchiveObjectWithData(sessionData!) as SPTSession?
                
                self.audioStreamingController.loginWithSession(session) { (error) in
                    let url = NSURL.URLWithString(newSong.uri!)
                    
                    SPTRequest.requestItemAtURI(url, withSession: session) { (error, object) in
                        self.audioStreamingController.playTrackProvider(object as SPTTrackProvider, callback: nil)
                        
                        self.audioStreamingController.setIsPlaying(true, nil)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.audioStreamingController.delegate = self;
    }

    override func viewWillAppear(animated: Bool) {
        
        //    NSLog("\(self.song.description()) imgUrl: \((self.song.coverArtUrl?.absoluteString)!)")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    }

    
}
