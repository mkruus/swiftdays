//
//  AppDelegate.swift
//  SwiftDays
//
//  Created by Veronika Ekström on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import UIKit

let kClientId = "e6695c6d22214e0f832006889566df9c"
let kCallbackURL = NSURL.URLWithString("spotifyiossdkexample://")

let kSessionUserDefaultsKey = "kSessionUserDefaultsKey"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private func isLoginNeeded() -> Bool {
        var sessionData = NSUserDefaults.standardUserDefaults().objectForKey(kSessionUserDefaultsKey) as NSData?
        
        if(sessionData == nil) {
            return true
        }
        
        let session = NSKeyedUnarchiver.unarchiveObjectWithData(sessionData!) as SPTSession?
        
        if(session == nil) {
            return true;
        }
        
        return !session!.isValid()
    }
    
    private func openLoginPage() {
        let auth = SPTAuth.defaultInstance()
        let url = auth.loginURLForClientId(kClientId, declaredRedirectURL: kCallbackURL, scopes: [SPTAuthStreamingScope], withResponseType: "token")
        
        UIApplication.sharedApplication().openURL(url)
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {        
        UINavigationBar.appearance().barTintColor = UIColor(red:0.180, green:0.671, blue:0.447, alpha:1)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        if self.isLoginNeeded() {
            self.openLoginPage()
        }

        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String, annotation: AnyObject?) -> Bool {
        let canHandleURL = SPTAuth.defaultInstance().canHandleURL(url, withDeclaredRedirectURL: kCallbackURL)
        
        if canHandleURL {
            SPTAuth.defaultInstance().handleAuthCallbackWithTriggeredAuthURL(url) { (error, session)  in
                if error != nil {
                    println(error.description)
                }
                
                let sessionData = NSKeyedArchiver.archivedDataWithRootObject(session)
                NSUserDefaults.standardUserDefaults().setObject(sessionData, forKey: kSessionUserDefaultsKey)
            }
            
            return true
        }
        
        return false
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

