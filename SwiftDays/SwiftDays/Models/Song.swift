//
//  Song.swift
//  SwiftDays
//
//  Created by Veronika Ekström on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import UIKit

class Song:NSObject{
   
    let artist:String = ""
    let title:String = ""
    let coverArtUrl:NSURL? = nil
    let uri:String? = ""
    
    override convenience init () {
        self.init (artist: "", title: "", coverArtUrl: nil, uri:nil)
    }
    
    init (artist:String, title:String, coverArtUrl:NSURL?, uri:String?) {
        super.init()
        self.artist = artist
        self.title = title
        self.coverArtUrl = coverArtUrl
        self.uri = uri
    }
    func description()->String{
        return self.artist+" : "+self.title
    }
}
