//
//  SpotifyAPIClient.swift
//  SwiftDays
//
//  Created by Markus Kruusmägi on 25/09/14.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import UIKit



class SpotifyAPIClient: NSObject {
    
    typealias SpotifyCallback = Song -> ()
    
    let title:String = ""
    let artist:String = ""
    
    init(title:String, artist:String) {
        super.init()
        self.title = title
        self.artist = artist
    }
    
    func makeQuery(title:String, artist:String) -> String {
        let mainQuery = 	"http://api.spotify.com/v1/search/?q="
        //        https://api.spotify.com/v1/search/?q= artist:Jessie+J+Ariana+Grande+Nicki+Minaj track:Bang+Bang&type=track&limit=1
        
        let subQuery = "artist:" + artist + " track:"+title
        
        let settingsQuery = "&type=track&limit=1"
        
        return mainQuery + subQuery + settingsQuery
    }
    
    func query() -> String {
        return makeQuery(self.title, artist: self.artist)
    }
    
    func request(spotifyCallback: SpotifyCallback) {
        NSLog(self.query())
        
        let escapedUrlString = self.query().stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!;
        
        var request = NSURLRequest(URL: NSURL(string: escapedUrlString))
//        NSLog("%@", request)
        var res = ""
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) in
//            NSLog(NSString(data: data, encoding: NSUTF8StringEncoding))
            
            var parsingError: NSError?
            let jsonDictonary = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parsingError) as NSDictionary
            
            if (parsingError != nil) {
                println(parsingError)
            }
            
            let tracks = jsonDictonary["tracks"] as NSDictionary
            
            let items = tracks["items"] as NSArray
            
            var uri : String? = nil
            
            var imgUrl : String? = nil
            
            if items.count > 0 {
                
                let item = items[0] as NSDictionary
                
//                for it in item.allKeys {
//                    NSLog(it as String)
//                }
                
                let album = item["album"] as NSDictionary
                
                let images = album["images"] as NSArray
                
                imgUrl = images[0]["url"] as? String
                
//                for img in images {
//                    let url = img["url"] as String
//                    let height = img["height"] as Int
//                    if height == 640 {
//                        imgUrl = url
//                    }
//                }
                
                uri = item["uri"] as? String
                let id = item["id"] as String
//                if let uriString = uri {
//                    NSLog("URI : %@",uriString)
//                }
//                if let imgUrlString = imgUrl {
//                    NSLog("ImgUrl : %@",imgUrlString)
//                }
                
//                NSLog("id : %@",id)
                
                //let jsonSongs = jsonDictonary["songs"] as NSArray
                
            }
            let coverArtUrl = (imgUrl != nil) ? NSURL.URLWithString(imgUrl!) : nil
            
            
            let song = Song(artist: self.artist, title: self.title, coverArtUrl: coverArtUrl, uri:uri)
            
            
            spotifyCallback(song)
        }
        
        
        
    }
    
}
