//
//  PlaylistDataSource.swift
//  SwiftDays
//
//  Created by Veronika Ekström on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import UIKit

class PlaylistDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    let cellId:String =  "cellIdentifier"
    
    
    var tableView: UITableView?
    
    var songs: [Song] = [Song]() {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.songs.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellId, forIndexPath: indexPath) as UITableViewCell
        cell.backgroundColor = UIColor(red: 0.137, green: 0.486, blue: 0.333, alpha:1)
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.textLabel?.text = self.songs[indexPath.row].description()
        return cell
    }
    
    func itemAtIndexPath(indexPath:NSIndexPath)->Song {
        return self.songs[indexPath.row]
    }
}
