//
//  SongInformationView.swift
//  SwiftDays
//
//  Created by Veronika Ekström on 2014-09-25.
//  Copyright (c) 2014 Veronika Ekström. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImageWithURL(url:NSURL) {
        
        let request = NSURLRequest(URL: url)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            var coverArtImage = UIImage(data:data)
            self.image = coverArtImage
        }
    }
}

class SongInformationView: UIView {

    @IBOutlet var songTitleLabel:UILabel? = UILabel()
    @IBOutlet var songArtistLabel:UILabel? = UILabel()
    @IBOutlet var coverArtImageView:UIImageView? = UIImageView()
    
    var spotifyUri : NSURL? = nil
    
    var coverArtImageURL:NSURL? = nil
    
    var didClickPlayCallback: (AnyObject -> Void)? = nil
    
    var song:Song = Song(){
        didSet {
            self.songTitleLabel?.text = self.song.title
            self.songArtistLabel?.text = self.song.artist
            self.coverArtImageURL = self.song.coverArtUrl
            if self.song.uri != nil {
                self.spotifyUri = NSURL.URLWithString("spotify://"+self.song.uri!)
            }
            if self.song.coverArtUrl != nil {
                self.coverArtImageView?.loadImageWithURL(coverArtImageURL!)
            }
        }
    }
    
//    override convenience init(frame: CGRect) {
//        self.init(frame:CGRectZero)
//    }
    

    @IBAction func didClickPlay(sender:AnyObject) {
        NSLog("Play")

        self.didClickPlayCallback?(sender);
    }
    
    @IBAction func didClickSpotify(sender:AnyObject) {
        NSLog("Spotify")
        if self.spotifyUri != nil {
            UIApplication.sharedApplication().openURL(self.spotifyUri!)
        }
        
        
    }
        
    override func layoutSubviews() {
        self.songTitleLabel?.sizeToFit()
        self.songArtistLabel?.sizeToFit()
    }
}

